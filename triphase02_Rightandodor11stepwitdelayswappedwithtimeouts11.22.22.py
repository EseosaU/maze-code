import RPi.GPIO as GPIO
import time
import multimaestro
import os
import csv
import numpy as np
import random
###L reward only
##if tdelay not reached, odor turns off after 3 sec (or number in odorwait var)9.29.22
##still gotta get odor to turn off when they dont hold long enough... 9.27.22
#8.31.22 make it repeated Left choice reward. both choice doors open,only left is rewarded
##right is wrong, no odor 
#6.14.22 TRIAL NUM, SLEEP, 0 CATCH
#6.1.22 attempting to add odor details to log
#5.23.22--- still working on this: test biased func with catch at 25% and proof read for correct vars
##UPDATED 5.16.22 to pseudorandomize trial type
##UPDATED 1.18.22 to record poke length at .25s interval
#UPDATED 10.27.22 to swap red and white syringe pumps
#UPDATED 11.22.22 with timeouts

#next things
##only left with amyl acetate, 1step trials
##only right, ethyl hex, 1step trials
##random es has the code check?
##only Right no odor (this but with right)

#my variables uwu
#timings in seconds
startdelay= 1.50
T_trial= time.time() +60
minute = 60
timeout = 5
#change T_sesh number for length of entire session in minutes
T_sesh=time.time() + 40*minute
#pi pins
IR_0 =17  
IR_1 =22 
IR_2 =6 
IR_3 =19 #ithink this is extra
cam =16

#polo pins
water0 =0
water1 =1
water2 =2
water3 =3 #ithink this is extra

door0 =4 
door1 =5
door2 =6
door3 =7
door4 =8

led0 =9
led1 =10
led2 =11
led3 =12

odor1 =13 #0,1,2 in JM notes
odor2 =14
odor3 =15
bank1 =16
bank2 =17
bank3 =18
odortrig =19

odorout =20
waterout =21
ledout =22
doorout =23


#set up
polo = multimaestro.Controller()
GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)
GPIO.setup([IR_0,IR_1,IR_2,IR_3], GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(cam,GPIO.OUT) #trigger cam

polo.setSpeed(water0, 50)
polo.setSpeed(water1, 50)
polo.setSpeed(water2, 50)
polo.setSpeed(water3, 50)
polo.setAccel(water0, 10)
polo.setAccel(water1, 10)
polo.setAccel(water2, 10)
polo.setAccel(water3, 10)
start = polo.getPosition(0)

on = 14000
off = 0
opened = 0
canclose = 7000

re0,re1,re2,poke0,poke1,poke2= 0,1,2,3,4,5
now = time.time()
datetime = str(time.strftime("%d.%b.%y_%H.%M"))

trialnum=0
Lcount,Rcount=1,1
catchcount=1




def tdelayinc():
    global Lcount, startdelay
    tdelay=startdelay #if Lcount<=1 else ((startdelay+0.05) if Lcount>1 and Lcount<=2 else (startdelay+0.10)) #if Lcount>=3 and Lcount<4 else (startdelay+0.15) if Lcount>=4 and Lcount<5 else (startdelay+0.20) if Lcount>=5 and Lcount<6 else (startdelay+0.25) if Lcount>=6 and Lcount<7 else (startdelay+0.30) if Lcount>=7 and Lcount<8 else (startdelay+0.35) if Lcount>=8 and Lcount<9 else (startdelay+0.40) if Lcount>=9 and Lcount<10 else (startdelay+0.45) if Lcount>=10 and Lcount<11 else (startdelay+0.50)) #if Lcount>=11 and Lcount<12 else (startdelay+0.55) if Lcount>=12 and Lcount<13 else (startdelay+0.55) if Lcount>=13 and Lcount<14 else (startdelay+0.60) if Lcount>=14 and Lcount<15 else (startdelay+0.65) if Lcount>=15 and Lcount<16 else (startdelay+0.70) if Lcount>=16 and Lcount<17 else (startdelay+0.75) if Lcount>=17 and Lcount<18 else (startdelay+0.80) if Lcount>=18 and Lcount<19 else (startdelay+0.85) if Lcount>=19 and Lcount<20 else (startdelay+0.90) if Lcount>=20 and Lcount<21 else (startdelay+0.95) if Lcount>=21 and Lcount<22 else (startdelay+1.00))
    #tdelay=0.5 if NScount<15 else (0.6 if NScount>=15 and NScount<30 else (0.7 if NScount>=30 and NScount<45 else 0.8))
    print(tdelay)
    time.sleep(0.0001)
    return tdelay

def opendoor(doorx):
    polo.setTarget(doorx, opened)
    polo.setTarget(doorout, on)
    time.sleep(.1)
    polo.setTarget(doorx, canclose)
    polo.setTarget(doorout, off)
    
def openalldoors():
    opendoor(door0)
    opendoor(door1)
    opendoor(door2)
    opendoor(door3)
    opendoor(door4)

def allled():
    polo.setTarget(led0,on)
    polo.setTarget(led1,on) 
    polo.setTarget(led2,on)
    polo.setTarget(led3,on)
    polo.setTarget(ledout,on)
    time.sleep(.5)
    polo.setTarget(ledout,off)
    
def allledoff():
    polo.setTarget(led0,off)
    polo.setTarget(led1,off) 
    polo.setTarget(led2,off)
    polo.setTarget(led3,off)
    polo.setTarget(ledout,off)
    
def ledon(ledx):
    polo.setTarget(ledx,on)
    polo.setTarget(ledout,on)
    time.sleep(.5)
    polo.setTarget(ledout,off)
def ledoff(ledx):
    polo.setTarget(ledx,off)
    polo.setTarget(ledout,on)
    time.sleep(.5)
    polo.setTarget(ledout,off)
    
def reward(waterx):
    polo.setTarget(waterx, opened)
    polo.setTarget(waterout, on)
    time.sleep(1.5)
    polo.setTarget(waterx, canclose)
    polo.setTarget(waterout, off)

def changes(reX,x,rewardx):
    global odorx
    listo=[0,0,0,0,0,0]
    listo[reX]=1
    listo[x]=1
    now=time.time()
    file.write(str(now)+","+str(listo[0])+","+str(listo[1])+","+str(listo[2])+","+str(listo[3])+","+str(listo[4])+","+str(listo[5])+","+str(phasex)+","+str(odorx)+","+str(rewardx)+","+str(trialnum)+","+str(tdelay)+"\n")

def cam_inscotrig():
    GPIO.output(cam, GPIO.HIGH)
    polo.setTarget(ledout,on)
    time.sleep(0.75)
    GPIO.output(cam,GPIO.LOW)
    polo.setTarget(ledout,off)
def biased(): #trying to make it Left only
    global Lcount,Rcount,catchcount
    biasL = Lcount / (Lcount+Rcount) - 0
    p_Left = 100 - biasL
    
    biasCatch = catchcount / (Lcount+Rcount+catchcount) - 0 #25% of trials will be catch
    p_catch = 0 - biasCatch #25% of trials will be catch 
    
    next_reward_at_L = np.random.uniform(low=0,high=1,size=1) < p_Left #if true E and if false W
    catchyes = np.random.uniform(low=0,high=1,size=1) < p_catch
    if next_reward_at_L == True:
        phasex=0
        time.sleep(.1)
        print(phasex,catchyes)
        return phasex,catchyes
    else:#this must change
        phasex=1
        time.sleep(.1)
        print(phasex,catchyes)
        return phasex,catchyes
    
def bankswitch():
    polo.setTarget(bank1, off)
    polo.setTarget(bank2, off)
    polo.setTarget(bank3, off)
def odorAorB():
    if  next_reward_at_L == True:
        odorx=odor1
        time.sleep(.1)
        return odorx
    else:
        odorx=odor2
        time.sleep(.1)
        return odorx
def odorxbankx(odorx,bankx):
    bankswitch()
    polo.setTarget(odor3, off)
    polo.setTarget(odorx, on)
    polo.setTarget(bankx, on)
    polo.setTarget(odorout, on)
    polo.setTarget(odortrig, on)
    time.sleep(5)
    polo.setTarget(odorx, off)
    #polo.setTarget(bankx, off)
    polo.setTarget(odor3, on)
    time.sleep(2)
    return odorx
    #polo.setTarget(odortrig, on)
    #time.sleep(1)
    #polo.setTarget(odortrig, off)
#1s odor trig at t0, door open if tdelay reached, if not start over 

def odorinit(odorx,bankx):
    bankswitch()
    polo.setTarget(odor3, off)
    polo.setTarget(odorx, on)
    polo.setTarget(bankx, on)
    polo.setTarget(odorout, on)
    polo.setTarget(odortrig, on)
    time.sleep(.01)
    return odorx
def odorfin(odorx):
    #time.sleep(0.2) #maybe dont need, maybe just here or just badpoke() (definitely one of them)
    #bankswitch()
    polo.setTarget(odorout, off)
    polo.setTarget(odorx, off)
    polo.setTarget(odor3, on)
    time.sleep(0.1)
    polo.setTarget(odortrig, on)
    time.sleep(0.1)#mayb no need
    print("fini")
tdelay=tdelayinc()
misstd=0
tempdelay=0
odorx=0
phasex,catchyes= biased()
subphase= 0 
#might have to do a catchyes= biased() and have that returned as well :P
#allled()
#openalldoors()
#odorAorB()
ledon(led0)
cam_inscotrig()
    
bb= "/home/pi/Desktop/"+ datetime +".csv"
with open("/home/pi/Desktop/"+ datetime +".csv", "a")as file:
    file.write("Time,re0,re1,re2,poke0,poke1,poke2,phasex,odor,reward,trialnum,tdelay"+ "\n") 
    file.write(str(now)+",0,0,0,0,0,0,"+str(phasex)+ ",0,0,0,0\n")
    while time.time() < T_sesh:
        if ((GPIO.input(IR_0) == False) and subphase==0): 
            changes(re0,poke0,0)
            print("poke at IR_0")
            odorx=odorinit(odor1,bank3)
            changes(re0,poke0,0)
            odorwait= time.time() + 3 ###change for how long u want the odor in seconds
            pokewait= time.time() + (tdelay+tempdelay)
            while (GPIO.input(IR_0) == False):
                if time.time()<pokewait:
                    changes(poke0,poke0,0)
                    time.sleep(0.1)
                    print("ooop")
                else:
                    break
            if time.time()<pokewait:
                print("bad")
                misstd+=1
                time.sleep(odorwait-pokewait)
                odorfin(odorx)
                #change this !
                if misstd>4: #4 misses--> down 0.1, go back up after
                    tempdelay= tempdelay-0.1
                    tdelay=tdelay+tempdelay
                    misstd=misstd-5 #change this when u change misstd !
                    print(tdelay)
                else:
                    continue
            if time.time()>=pokewait: #door open, good
                changes(poke0,poke0,0)
                ledoff(led0)
                print("good")
                tdelay=tdelayinc()
                opendoor(door0)
                opendoor(door4)
                opendoor(door3)
                tempdelay=0
                subphase =1
                time.sleep(odorwait-pokewait)
            if time.time()>odorwait:
                odorfin(odorx)
                odorx=0


        if GPIO.input(IR_1)== False and subphase==1: #poke R correct/reward, return next trial
            trialnum+=1
            changes(re1,poke1,1)
            print("poke at IR_1")
            subphase =0###return to next trial 
            reward(water1) ###
            ledoff(led1)
            time.sleep(2)
            opendoor(door1)
            ledon(led0)
            Lcount+=1
            phasex,catchyes=biased()
            #equal random choice
        if GPIO.input(IR_2)== False and subphase==1: #poke L incorrect/no reward, return next trial 
            trialnum+=1
            changes(re2,poke2,0)
            print("wrong poke at IR_2")
            subphase =0#return to next trial
            #reward(water1)
            #ledoff(led1)
            time.sleep(timeout) #if you want timeout to delay them getting back to the init port, uncomment
            opendoor(door2)
            ledon(led0)
            Lcount+=1
            phasex,catchyes=biased()
            #time.sleep(timeout) #if you want timeout to delay the init port from responding, uncomment this line
            #equal random choice
            
        if GPIO.input(IR_0)== False and subphase==1: #all IR0 pokes after initial are recorded
            changes(poke0,poke0,0)
            print("repeat IR_0")
            time.sleep(.25)
        if GPIO.input(IR_2)== False and (subphase==2 or subphase == 0): #all R pokes after reward at L are recorded
            changes(poke2,poke2,0)
            print("repeat IR_2")
            time.sleep(.25)
        if GPIO.input(IR_1)== False and subphase==0: #all L after wrong choice are recorded
            changes(poke1,poke1,0)
            print("repeat IR_1")
            time.sleep(.25)
        
file.close()       
allledoff()
print(Lcount,Rcount)
polo.setTarget(ledout,off)
polo.setTarget(waterout,off)
polo.setTarget(odorout,off)
polo.setTarget(doorout,off)

GPIO.cleanup()

        
mouseid= input('input mouse id: ') +'_'
trainday= input('input training day: ') +'_'          
        
os.rename(bb, "/home/pi/Desktop/"+ str(mouseid) + str(trainday) + str(datetime) + ".csv")   


#A ---> EVERYTHING OPENS (EXCEPT EXITS) --> CORRECT = L, INCORRECT = R (GOES HOME)
#50:50 B--> RIGHT VERSION

###make this script work properly
###no odor-- 1st. homecage poke, Lreward home random L and R
###2 trial-- L not rward, not poke needed--- both doors open== then opposite to get reward,, wrong doors close behind and open to finish no reward
######>>>random LR
    
    