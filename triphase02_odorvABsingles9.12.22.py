import RPi.GPIO as GPIO
import time
import multimaestro
import os
import csv
import numpy as np
import random
###poke at 0, get odor A--> Reward Left, get odorB--> REward Right 
##9.12.22 added odor,reward,trialnum cols cool
#test again but I think functional 5.23.22
#A ---> EVERYTHING OPENS (EXCEPT EXITS) --> CORRECT = L, INCORRECT = R (GOES HOME)
#50:50 B--> RIGHT VERSION
##UPDATED 5.16.22 to pseudorandomize trial type
##UPDATED 1.18.22 to record poke length at .25s interval


#my variables uwu
#timings in seconds
T_trial= time.time() +60
minute = 60
#change T_sesh number for length of entire session in minutes
T_sesh=time.time() + 1*minute
#pi pins
IR_0 =17  
IR_1 =22 
IR_2 =6 
IR_3 =19 #ithink this is extra
cam =16

#polo pins
water0 =1
water1 =0
water2 =2
water3 =3 #ithink this is extra

door0 =4 
door1 =5
door2 =6
door3 =7
door4 =8

led0 =9
led1 =10
led2 =11
led3 =12

odor1 =13 #0,1,2 in JM notes
odor2 =14
odor3 =15
bank1 =16
bank2 =17
bank3 =18
odortrig =19

odorout =20
waterout =21
ledout =22
doorout =23


#set up
polo = multimaestro.Controller()
GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)
GPIO.setup([IR_0,IR_1,IR_2,IR_3], GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(cam,GPIO.OUT) #trigger cam

polo.setSpeed(water0, 50)
polo.setSpeed(water1, 50)
polo.setSpeed(water2, 50)
polo.setSpeed(water3, 50)
polo.setAccel(water0, 10)
polo.setAccel(water1, 10)
polo.setAccel(water2, 10)
polo.setAccel(water3, 10)
start = polo.getPosition(0)

on = 14000
off = 0
opened = 0
canclose = 7000

re0,re1,re2,poke0,poke1,poke2= 0,1,2,3,4,5
now = time.time()
datetime = str(time.strftime("%d.%b.%y_%H.%M"))

trialnum=0
Lcount,Rcount=1,1

    
def opendoor(doorx):
    polo.setTarget(doorx, opened)
    polo.setTarget(doorout, on)
    time.sleep(.1)
    polo.setTarget(doorx, canclose)
    polo.setTarget(doorout, off)
    
def openalldoors():
    opendoor(door0)
    opendoor(door1)
    opendoor(door2)
    opendoor(door3)
    opendoor(door4)

def allled():
    polo.setTarget(led0,on)
    polo.setTarget(led1,on) 
    polo.setTarget(led2,on)
    polo.setTarget(led3,on)
    polo.setTarget(ledout,on)
    time.sleep(.5)
    polo.setTarget(ledout,off)
    
def allledoff():
    polo.setTarget(led0,off)
    polo.setTarget(led1,off) 
    polo.setTarget(led2,off)
    polo.setTarget(led3,off)
    polo.setTarget(ledout,off)
    
def ledon(ledx):
    polo.setTarget(ledx,on)
    polo.setTarget(ledout,on)
    time.sleep(.5)
    polo.setTarget(ledout,off)
def ledoff(ledx):
    polo.setTarget(ledx,off)
    polo.setTarget(ledout,on)
    time.sleep(.5)
    polo.setTarget(ledout,off)
    
def reward(waterx):
    polo.setTarget(waterx, opened)
    polo.setTarget(waterout, on)
    time.sleep(1)
    polo.setTarget(waterx, canclose)
    polo.setTarget(waterout, off)

def changes(reX,x,rewardx):
    #global odorx
    listo=[0,0,0,0,0,0]
    listo[reX]=1
    listo[x]=1
    now=time.time()
    file.write(str(now)+","+str(listo[0])+","+str(listo[1])+","+str(listo[2])+","+str(listo[3])+","+str(listo[4])+","+str(listo[5])+","+str(phasex)+","+str(odorx)+","+str(rewardx)+","+str(trialnum)+"\n")

def cam_inscotrig():
    GPIO.output(cam, GPIO.HIGH)
    polo.setTarget(ledout,on)
    time.sleep(0.75)
    GPIO.output(cam,GPIO.LOW)
    polo.setTarget(ledout,off)
def biased(): #idk exactly what the trial types look like atm
    global Lcount,Rcount
    biasL = Lcount / (Lcount+Rcount) - 0.5
    p_Left = 0.5 - biasL
    
    next_reward_at_L = np.random.uniform(low=0,high=1,size=1) < p_Left #if true E and if false W
    if next_reward_at_L == True:
        phasex=0
        time.sleep(.1)
        print(phasex)
        return phasex
    else:#this must change
        phasex=1
        time.sleep(.1)
        print(phasex)
        return phasex
def bankswitch():
    polo.setTarget(bank1, off)
    polo.setTarget(bank2, off)
    polo.setTarget(bank3, off)
def odorAorB():
    if  next_reward_at_L == True:
        odorx=odor1
        time.sleep(.1)
        return odorx
    else:
        odorx=odor2
        time.sleep(.1)
        return odorx
def odorxbankx(odorx,bankx):
    bankswitch()
    polo.setTarget(odor3, off)
    polo.setTarget(odorx, on)
    polo.setTarget(bankx, on)
    polo.setTarget(odorout, on)
    polo.setTarget(odortrig, on)
    time.sleep(5)
    polo.setTarget(odorx, off)
    #polo.setTarget(bankx, off)
    polo.setTarget(odor3, on)
    time.sleep(.1)
    return odorx
    #polo.setTarget(odortrig, on)
    #time.sleep(1)
    #polo.setTarget(odortrig, off)
  


phasex= biased()
subphase= 0 

#allled()
#openalldoors()
odorx=0
ledon(led0)
cam_inscotrig()
    
bb= "/home/pi/Desktop/"+ datetime +".csv"
with open("/home/pi/Desktop/"+ datetime +".csv", "a")as file:
    file.write("Time,re0,re1,re2,poke0,poke1,poke2,phasex,odor,reward,trialnum"+ "\n") 
    file.write(str(now)+",0,0,0,0,0,0,"+str(phasex)+",0,0\n")
    while time.time() < T_sesh:
        #might need to change position
        if phasex==0: #A --> LEFT--> A LEFT trial #MAKE B VERSION
            #ledon(led0)
            if GPIO.input(IR_0) == False and subphase==0: #waiting for poke at IR0
                print("poke at IR_0")
                subphase =1
                ledoff(led0)
                odorx=odorxbankx(odor2,bank3)
                changes(re0,poke0,0)
                odorx=0
                time.sleep(2)
                opendoor(door0)
                opendoor(door4)
                opendoor(door3)
            
            if GPIO.input(IR_2)== False and subphase==1: #poke L correct/reward, return next trial
                changes(re2,poke2,1)
                print("poke at IR_2")
                subphase =0###return to next trial 
                reward(water2) ###
                ledoff(led2)
                opendoor(door2)
                ledon(led0)
                trialnum+=1
                Lcount+=1
                phasex=biased()
                #equal random choice
            if GPIO.input(IR_1)== False and subphase==1: #poke R incorrect/no reward, return next trial 
                changes(poke1,poke1,0)
                print("wrong poke at IR_1")
                subphase =0#return to next trial
                #reward(water1)
                #ledoff(led1)
                opendoor(door1)
                ledon(led0)
                trialnum+=1
                Lcount+=1
                phasex=biased()
                #equal random choice
                
            if GPIO.input(IR_0)== False and subphase==1: #all IR0 pokes after initial are recorded
                changes(poke0,poke0,0)
                print("repeat IR_0")
                time.sleep(.15)
            if GPIO.input(IR_2)== False and subphase==0: #all L pokes after odor at L are recorded
                changes(poke2,poke2,0)
                print("repeat IR_2")
                time.sleep(.15)
            if GPIO.input(IR_1)== False and subphase==0: #all R after wrong choice are recorded
                changes(poke1,poke1,0)
                print("repeat IR_1")
                time.sleep(.15)
        if phasex==1: #B--> RIGHT trial type
            if GPIO.input(IR_0) == False and subphase==0: #waiting for poke at 0 
                print("poke at IR_0")
                subphase =1
                odorx=odorxbankx(odor1,bank3)
                changes(re0,poke0,0)
                odorx=0
                ledoff(led0)
                time.sleep(2)
                #reward(water0)#do we want reward here
                opendoor(door0)
                opendoor(door4)
                opendoor(door3)
            if GPIO.input(IR_1)== False and subphase==1: #poke R correct/reward, return next trial
                changes(re1,poke1,1)
                print("poke at IR_1")
                subphase =0###return to next trial 
                reward(water1) ###
                ledoff(led1)
                opendoor(door1)
                ledon(led0)
                trialnum+=1
                Rcount+=1
                phasex=biased()
                #equal random choice
            if GPIO.input(IR_2)== False and subphase==1: #poke L incorrect/no reward, return next trial 
                changes(poke2,poke2,0)
                print("wrong poke at IR_2")
                subphase =0#return to next trial
                #reward(water1)
                #ledoff(led1)
                opendoor(door2)
                ledon(led0)
                trialnum+=1
                Rcount+=1
                phasex=biased()
                #equal random choice
                
            if GPIO.input(IR_0)== False and subphase==1: #all IR0 pokes after initial are recorded
                changes(poke0,poke0,0)
                print("repeat IR_0")
                time.sleep(.15)
            if GPIO.input(IR_1)== False and subphase == 0: #all L pokes after odor at L are recorded
                changes(poke1,poke1,0)
                print("repeat IR_1")
                time.sleep(.15)
            if GPIO.input(IR_2)== False and subphase==0: #all R after wrong choice are recorded
                changes(poke2,poke2,0)
                print("repeat IR_2")
                time.sleep(.15)
        
file.close()       
allledoff()
print(Lcount,Rcount)
polo.setTarget(ledout,off)
polo.setTarget(waterout,off)
polo.setTarget(odorout,off)
polo.setTarget(doorout,off)

GPIO.cleanup()

        
mouseid= input('input mouse id: ') +'_'
trainday= input('input training day: ') +'_'          
        
os.rename(bb, "/home/pi/Desktop/"+ str(mouseid) + str(trainday) + str(datetime) + ".csv")   


#A ---> EVERYTHING OPENS (EXCEPT EXITS) --> CORRECT = L, INCORRECT = R (GOES HOME)
#50:50 B--> RIGHT VERSION

###make this script work properly
###no odor-- 1st. homecage poke, Lreward home random L and R
###2 trial-- L not rward, not poke needed--- both doors open== then opposite to get reward,, wrong doors close behind and open to finish no reward
######>>>random LR
    